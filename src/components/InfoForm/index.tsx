import axios from 'axios';
import {useState, useEffect} from 'react';
import { useRouter } from 'next/router';
import FormBar from '../FormBar';
import Toast from '../Toast';
import { infoFormType, infoType, createInfoType } from '@/utils/types';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { esES } from '@mui/x-date-pickers'
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import utc from 'dayjs/plugin/utc';
import dayjs from 'dayjs';
import 'dayjs/locale/es-mx';
import { 
    Box,
    RadioGroup,
    Paper,
    Typography,
    Grid,
    FormGroup,
    FormControl,
    Radio,
    TextField,
    InputLabel,
    FormControlLabel,
    OutlinedInput,
    Checkbox,
    Divider,
    FormLabel,
    FormHelperText
} from '@mui/material';
dayjs.extend(utc);

const labelStyle = {
    backgroundColor: 'white',
    padding: '4px 8px',
    borderRadius:'4px'
}
const InfoForm = (props : infoFormType) => {
    const router = useRouter();
    // const isDisabled = props.isDisabled;    //Comportamiento inicial del formulario
    const [isDisabled, setIsDisabled] = useState<boolean>(props.isDisabled)   // Activa / Desactiva las forms para editarlos
    const [toastOpen, setToastOpen] = useState<boolean>(false)
    const [succesToastOpen, setSuccesToastOpen] = useState<boolean>(false)
    const [myInfo, setMyInfo] = useState<infoType>(props?.data ? props?.data : createInfoType());
    const [shrink, setShrink] = useState<boolean | undefined>(isDisabled?true:undefined) // Controla el comportamiento de los InputLabel
    const [otroActividades, setOtroActividades] = useState<boolean>(true);  // Activa / Desactiva el input para el radio "Otro" en actividades
    const [otroRequerimientos, setOtroRequerimientos] = useState<boolean>(isDisabled);   // Activa/desactiva el input para el radio "Otro" en requerimientos
    
    const [valueActividades, setValueActividades] = useState<string>("");
    const [valueModo, setValueModo] = useState<string>("");

    // Actualiza la info del RadioGroup de Actividades
    const handleActividades = (event: React.ChangeEvent<HTMLInputElement>) => {
        if(!!!isDisabled) {
            // console.log("Changin to:", event.target.value)
            if((event.target as HTMLInputElement).value === "Otro") {
                setOtroActividades(false);
            } else {
                setOtroActividades(true);
            }
            setValueActividades(event.target.value);
            setMyInfo({
                ...myInfo,
                [event.target.name] : event.target.value
            })
        }
    };

    // activa /desactiva la casilla de Requerimientos Extras
    const handleReqs = () => {
        setOtroRequerimientos(!otroRequerimientos);
    }

    // Actualiza la info del RadioGroup de Modo
    const handleModo = (event: React.ChangeEvent<HTMLInputElement>) => {
        // console.log(event.target)
        setValueModo((event.target as HTMLInputElement).value);
        setMyInfo({
            ...myInfo,
            [event.target.name] : event.target.value
        })
    }

    const handleDisable = (event: React.ChangeEvent<HTMLInputElement>) => {
        setIsDisabled(!isDisabled);
    }
    // Actualiza la info del estado que manejan los Inputs
    const onType = (e :  React.ChangeEvent<HTMLInputElement>) => {
        // console.log("CREDENTIALS: ", credentials);
        setMyInfo({
            ...myInfo,
            [e.target.name]: e.target.value
        })
    }

    const handleToastClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
        return;
        }

        if(toastOpen) setToastOpen(false);
        // if(succesToastOpen) setToastOpen(false)
    };

    // Actualiza la info del estado que manejan los Checkbox
    const onCheck = (event: any) => {
        setMyInfo({
          ...myInfo,
          [event.target.name]: event.target.checked,
        });
      };

    const onDate = (event: any | null, type:string):void => {
        // console.log("PARSE:", dayjs(event['$d']).utc().local().format())
        if(type==='init') {
            setMyInfo({
                ...myInfo,
                fechaInicioGlobal: dayjs(event?.$d).utc().local().format(),
              });
        } else if(type==='final') {
            setMyInfo({
                ...myInfo,
                fechaFinalGlobal: dayjs(event?.$d).utc().local().format(),
              });
        }
        
      };
    // Controla el comportamiento de los labels en los inputs, necesario para que la animacion de los labels responda correctamente
    useEffect(()=>{
        if(isDisabled === true) setShrink(true);
        else setShrink(undefined);
    }, [isDisabled])


    // Configura la info inicial si viene desde el front
    useEffect(()=>{
        if(props?.data)
            setMyInfo(props?.data);
    }, [props?.data])

    // Establece el estado de los RadioGroup al montar el componente
    useEffect(()=>{
        // console.log("INFO: ", myInfo)
        if(myInfo.tipoActividad)  {
            setValueActividades(myInfo.tipoActividad)
        }
        if(myInfo.modo)  {
            setValueModo(myInfo.modo)
        }
    }, [myInfo])

    const saveInfo = async () => {
        try{
            // console.log("INSAVE")
            const response = await axios.post('/api/talk/saveInfo', myInfo);
            if(response.status === 200) {
                router.push({
                      pathname: `/colaboraciones/${response.data.slug}`,
                      query: { saved: true },
                    })
            } else {
                console.error("La informacion es insuficiente");
            }
        }catch(err) {
            setToastOpen(true);
            console.error(err);
        }
    }
    const updateInfo = async () => {
        try{
            // console.log("INPUDATE")
            const response = await axios.post('/api/talk/updateInfo', myInfo);
            if(response.status === 200) {
                router.push({
                    pathname:`/colaboraciones/${response.data.slug}`,
                    query: { saved :true}
                })
                // router.push({
                //     pathname: `/colaboraciones/${slugify(myInfo.titulo)}`,
                //     query: { saved :true}
                // })
            } else {
                console.log("La informaciones insuficiente, asegurate de llenar los elementos requeridos");
            }
        }catch(err) {
            setToastOpen(true);
            console.error(err);
        }
    }
    return(
    <LocalizationProvider 
        dateAdapter={AdapterDayjs} 
        adapterLocale="es-mx" s
        localeText={esES.components.MuiLocalizationProvider.defaultProps.localeText}
        key={router.asPath} 
    >
        <Toast isOpen={toastOpen} message={'Completa los campos requeridos(Titulo, Descripción y Contenido)'} handleClose={handleToastClose}/>
        <Box marginTop={4} marginBottom={12} sx={{padding:{xs: '0 1rem', md:'0 2rem'}, maxWidth:'1800px'}}>
            <Paper elevation={0} sx={{padding:{xs:'0 0.5rem', sm:'0 1rem', md:'0 2rem'}}}>
                {props?.children?props?.children:<></>}
                <Box sx={{ padding: '2rem 1rem 1rem'}}>
                    <Typography variant='h3'>Información básica</Typography>
                    <Typography variant='subtitle1'>Para los materiales de redes sociales</Typography>
                </Box>

                <Grid container  columnSpacing={3} sx={{
                    padding: {xs:'0 1rem', md: '0 0 0 1rem'}
                }}>
                    <Grid container item md={5.8} xs={12}>
                        <FormControl required fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel htmlFor="titulo" shrink={shrink} style={labelStyle}>Titulo</InputLabel>
                            <OutlinedInput id="titulo" value={myInfo.titulo} onChange={onType} name="titulo" />
                            <FormHelperText>Breve, de 1 a 5 palabras máximo</FormHelperText>
                        </FormControl>

                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="subtitulo" shrink={shrink}>Subtitulo</InputLabel>
                            <OutlinedInput id="subtitulo" value={myInfo.subtitulo} onChange={onType} name="subtitulo"/>
                            <FormHelperText>Breve, de 1 a 5 palabras máximo</FormHelperText>
                        </FormControl>

                        <FormControl fullWidth margin='normal'>
                            <FormLabel component="legend">Titulo de actividad</FormLabel>
                            <RadioGroup
                                sx={{flexDirection:{xs:'column', sm:'row'}}}
                                onChange={handleActividades}
                                value={valueActividades}
                            >    
                                <FormControlLabel
                                    value={"Talleres"}
                                    disabled={isDisabled}
                                    label="Taller"
                                    control={<Radio size={"small"} name={"tipoActividad"}/>}
                                />
                                <FormControlLabel
                                    value={"Conferencias"}
                                    disabled={isDisabled}
                                    label="Conferencia"
                                    control={<Radio size={"small"} name={"tipoActividad"}/>}
                                />
                                <FormControlLabel
                                    value={"Conciertos"}
                                    disabled={isDisabled}
                                    label="Concierto"
                                    control={<Radio size={"small"} name={"tipoActividad"}/>}
                                />
                                <FormControlLabel
                                    value={"Exhibiciones"}
                                    disabled={isDisabled}
                                    label="Exposición "
                                    control={<Radio size={"small"} name={"tipoActividad"}/>}
                                />
                                <FormGroup
                                    sx={{
                                        // backgroundColor:'red',
                                        // display:'flex',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        width: '100%',
                                    }}
                                >
                                    <FormControlLabel
                                        value={"Otro"}
                                        disabled={isDisabled}
                                        label="Otro"
                                        control={<Radio size={"small"}  name={"tipoActividad"}/>}
                                    />                                    
                                    <FormControl sx={{width:{xs:'80%', sm:'85%', md:'80%', lg:'85%'}}} disabled={otroActividades} margin={'dense'}>
                                        <InputLabel style={labelStyle} htmlFor="otroEspec" shrink={shrink}>Especifique</InputLabel>
                                        <OutlinedInput id="otroEspec" value={myInfo.tipoActividadOtro} onChange={onType} name="tipoActividadOtro"/>
                                    </FormControl>                            
                                </FormGroup>
                            </RadioGroup>
                        </FormControl>
                        
                        <FormControl fullWidth margin='normal'>
                            <FormLabel component="legend">Modo</FormLabel>
                            <RadioGroup
                                row
                                onChange={handleModo}
                                value={valueModo}
                            >    
                                <FormControlLabel
                                    value={"Presencial"}
                                    disabled={isDisabled}
                                    label="Presencial"
                                    control={<Radio size={"small"} name={"modo"}/>}
                                />
                                <FormControlLabel
                                    value={"Online"}
                                    disabled={isDisabled}
                                    label="En línea"
                                    control={<Radio size={"small"} name={"modo"}/>}
                                />
                                <FormControlLabel
                                    value={"Hibrido"}
                                    disabled={isDisabled}
                                    label="Hibrido"
                                    control={<Radio size={"small"} name={"modo"}/>}
                                />
                            </RadioGroup>
                        </FormControl>

                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="personaOrganizadora" shrink={shrink}>Comparte</InputLabel>
                            <OutlinedInput id="personaOrganizadora" value={myInfo.personaOrganizadora} onChange={onType} name="personaOrganizadora"/>
                            <FormHelperText>Nombre(s) de tallerista, artista, colectiva, conferencista...</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="personasObjetivo" shrink={shrink}>Dirigido a</InputLabel>
                            <OutlinedInput id="personasObjetivo" value={myInfo.personasObjetivo} onChange={onType} name="personasObjetivo"/>
                            <FormHelperText>Público al que va dirigida la actividad</FormHelperText>
                        </FormControl>
                            <Box sx={{display:'flex', width:'90%', alignItems:'center', justifyContent:'space-between'}}>
                            <FormControl disabled={isDisabled} margin={'normal'}>
                                <InputLabel style={labelStyle} htmlFor="cupo" shrink={shrink}>Cupo</InputLabel>
                                <OutlinedInput id="cupo" type={"number"} value={myInfo.cupo} onChange={onType} name="cupo"/>
                                <FormHelperText># de personas si hay cupo limitado</FormHelperText>
                            </FormControl>									
                            <FormControlLabel
                                sx={{marginLeft:'1rem'}}
                                disabled={isDisabled}
                                label="Necesita inscripcion"
                                control={<Checkbox size={"small"} checked={myInfo.tieneRegistro !== null ? myInfo.tieneRegistro : false} onChange={onCheck} name={"tieneRegistro"}/>}
                            />
                        </Box>
                    </Grid>

                    <Divider 
                        orientation={"vertical"}
                        flexItem
                        variant={"inset"} 
                        sx={{marginLeft:'1.5rem'}}
                    />  

                    <Grid item md={5.8} xs={12}>
                        <DateTimePicker
                            disabled={isDisabled}
                            label="Fecha de inicio"
                            onChange={e=>onDate(e, "init")}
                            value={!!myInfo?.fechaInicioGlobal ? dayjs(myInfo.fechaInicioGlobal) : dayjs()}
                            // onChange={onDate}
                            renderInput={(params) => <TextField
                                // onChange={onDate}
                                fullWidth {...params} 
                                margin={"normal"}
                                name={"fechaInicioGlobal"}/>}
                        />

                        <DateTimePicker
                            disabled={isDisabled}
                            onChange={e=>onDate(e, "final")}
                            label="Fecha de termino"
                            value={!!myInfo.fechaFinalGlobal ? dayjs(myInfo.fechaFinalGlobal) : dayjs()}
                            // onChange={handleFinalDate}
                            renderInput={(params) => <TextField fullWidth {...params} margin={"normal"}/>}
                        />
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="resumenSesiones" shrink={shrink}>Sesiones</InputLabel>
                            <OutlinedInput id="resumenSesiones" multiline minRows={3} maxRows={5} value={myInfo.resumenSesiones} onChange={onType} name="resumenSesiones"/>
                            <FormHelperText>{`Opcional - Ej. "Sesiones en línea: 23, 24 y 25 de mayo y sesión final presencial 26 de mayo"`}</FormHelperText>
                        </FormControl>                            
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="copy"  shrink={shrink}>Sugerencia de copy</InputLabel>
                            <OutlinedInput id="copy" multiline minRows={3} maxRows={6} value={myInfo.copy} onChange={onType} name="copy"/>
                            <FormHelperText>Texto breve, claro y atractivo (140 cc) que describa y sirva de invitación para la actividad.
                            Lo podemos usar en flyers, historias o tuit</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="urlDriveImages" shrink={shrink}>Imagenes</InputLabel>
                            <OutlinedInput id="urlDriveImages" value={myInfo.urlDriveImages} onChange={onType} name="urlDriveImages"/>
                            <FormHelperText>Link de la carpeta con imágenes</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="urlDriveLogos" shrink={shrink}>Logos</InputLabel>
                            <OutlinedInput id="urlDriveLogos" value={myInfo.urlDriveLogos} onChange={onType} name="urlDriveLogos"/>
                            <FormHelperText>Link de la carpeta con logos</FormHelperText>
                        </FormControl>
                    </Grid>

                </Grid>

                <Box sx={{ padding: '2rem 1rem 1rem'}}>
                    <Typography variant='h3'>Información ampliada</Typography>
                    <Typography variant='subtitle1'>Para sitio web y boletines</Typography>
                </Box>

                <Grid container  columnSpacing={3} sx={{
                    padding: '1rem',
                }}>
                    <Grid container item md={5.8} xs={12}>
                        <FormControl required fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="introduccion" shrink={shrink}>Descripción</InputLabel>
                            <OutlinedInput multiline rows={3} id="introduccion" value={myInfo.introduccion} onChange={onType} name="introduccion"/>
                            <FormHelperText>Introducción al tema, ¿en qué consiste la actividad?, 250 palabras máximo</FormHelperText>
                        </FormControl>
                        <FormControl required fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="contenido" shrink={shrink}>Contenido</InputLabel>
                            <OutlinedInput multiline minRows={4} maxRows={6} id="contenido" value={myInfo.contenido} onChange={onType} name="contenido"/>
                            <FormHelperText>Aquí puedes incluir temario, justificación, contexto u objetivos</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="semblanza" shrink={shrink}>Semblanza</InputLabel>
                            <OutlinedInput multiline minRows={4} maxRows={6} id="semblanza" value={myInfo.semblanza} onChange={onType} name="semblanza"/>
                            <FormHelperText>Breve, 200 palabras máximo Si hay más de una semblanza, pegar link a documento</FormHelperText>
                        </FormControl>                            
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="contacto" shrink={shrink}>Contacto</InputLabel>
                            <OutlinedInput multiline minRows={4} maxRows={6} id="contacto"  value={myInfo.contacto} onChange={onType} name="contacto"/>
                            <FormHelperText>Sitio web y redes sociales</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Divider 
                        orientation={"vertical"}
                        flexItem
                        variant={"inset"} 
                        sx={{marginLeft:'1.5rem'}}
                    />                        
                    <Grid item md={5.8} xs={12} >
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="coments" shrink={shrink}>Comentario</InputLabel>
                            <OutlinedInput multiline minRows={4} maxRows={6} id="coments"  value={myInfo.comentarios} onChange={onType} name="comentarios"/>
                            <FormHelperText>Opcional / Si crees que hay más información relevante, escríbela aquí</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                            <FormLabel component="legend">Requerimientos técnicos</FormLabel>
                            <FormGroup >
                                <FormControlLabel
                                    disabled={isDisabled}
                                    label="Mobiliario (mesas, sillas)"
                                    control={<Checkbox size={"small"} checked={myInfo.requerimientosMobiliario !== null ? myInfo.requerimientosMobiliario : false} onChange={onCheck} name={"requerimientosMobiliario"}/>}
                                />
                                <FormControlLabel 
                                    disabled={isDisabled}
                                    label="Internet"
                                    control={<Checkbox size={"small"} checked={myInfo.requerimientosInternet !== null ? myInfo.requerimientosInternet : false} onChange={onCheck} name={"requerimientosInternet"}/>}
                                />
                                <FormControlLabel 
                                    disabled={isDisabled}
                                    label="Hardware"
                                    control={<Checkbox size={"small"} checked={myInfo.requerimientosHardware !== null ? myInfo.requerimientosHardware : false} onChange={onCheck} name={"requerimientosHardware"}/>}
                                />
                                <FormControlLabel 
                                    disabled={isDisabled}
                                    label="Software"
                                    control={<Checkbox size={"small"} checked={myInfo.requerimientosSoftware !== null ? myInfo.requerimientosSoftware : false} onChange={onCheck} name={"requerimientosSoftware"}/>}
                                />
                                <FormControlLabel 
                                    disabled={isDisabled}
                                    label="Material de papelería"
                                    control={<Checkbox size={"small"} checked={myInfo.requerimientosPapeleria !== null ? myInfo.requerimientosPapeleria : false} onChange={onCheck} name={"requerimientosPapeleria"}/>}
                                />
                                <Box
                                    sx={{
                                        display:'flex',
                                        alignItems: 'center'
                                    }}
                                >
                                    <FormControlLabel 
                                        disabled={isDisabled}
                                        label="Otro"
                                        control={<Checkbox 
                                                    size={"small"}
                                                    checked={ myInfo.requerimientosOtro !== null ? myInfo.requerimientosOtro : false} 
                                                    onChange={(e)=> {
                                                        handleReqs()
                                                        onCheck(e)
                                                    }}
                                                    name={"requerimientosOtro"}
                                                />}
                                    />                                    
                                    <FormControl fullWidth disabled={!otroRequerimientos || isDisabled} margin={'dense'}>
                                        <InputLabel style={labelStyle} htmlFor="otroExtras" shrink={shrink}>Extras</InputLabel>
                                        <OutlinedInput id="otroExtras" value={myInfo.requerimientosOtroExtras} onChange={onType} name="requerimientosOtroExtras"/>
                                    </FormControl>                            
                                </Box>
                            </FormGroup>
                        </FormControl>
                        <FormControl fullWidth disabled={isDisabled} margin={'normal'}>
                            <InputLabel style={labelStyle} htmlFor="notas" shrink={shrink}>Notas extra</InputLabel>
                            <OutlinedInput id="notas" multiline minRows={4} maxRows={6} value={myInfo.notas} onChange={onType} name="notas"/>
                        </FormControl>                            
                    </Grid>
                </Grid>

                <FormBar
                    onEdit={handleDisable}
                    onSave={()=>{
                        if(props.isNew === true) {
                            saveInfo();
                        } else if(props.isNew === false){
                            updateInfo();
                        }
                    }}
                    isNew={props.isNew?props.isNew:false}
                    isFormDisabled={isDisabled}
                />
            </Paper>
        </Box>

    </LocalizationProvider>
    )
}

export default InfoForm;