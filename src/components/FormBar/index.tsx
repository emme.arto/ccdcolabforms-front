import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import SaveIcon from '@mui/icons-material/Save';

type FormBarType = {
  isFormDisabled?: boolean
  isNew?: boolean
  onSave?: Function
  onEdit?: Function
}

const FormBar = (props : FormBarType) => {
  const {isFormDisabled, onSave, onEdit} = props;

  const handleMouse = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
          elevation={1}
          position='fixed' 
          sx={{
            borderRadius:'12px',
            bottom:'16px',
            top: 'auto',
            width:{xs: '90%', md:'94%'},
            left: {xs: 'calc(5%)', md:'3%'},
            // backgroundColor: '#afda5f',
            background: '#CBF281 0% 0% no-repeat padding-box',
            // opacity: 0.65
          }}>
        <Toolbar sx={{display:'flex', alignItems:'center', justifyContent:'end'}}>
         
         {
          !!props.isNew ?
            <></> :
            <Button
              disableElevation
              sx={{
                margin:'0 2rem',
                backgroundColor: `${!!isFormDisabled ? '#afda5f' : 'none'}`,
                mixBlendMode: 'multiply',
                color: '#272727'
              }}
              variant={!!isFormDisabled ? 'contained' : 'outlined'}
              // color={!!isFormDisabled ? "secondary" : "info"}
              endIcon={<EditIcon />}
              onClick={()=>{
                if(onEdit!== undefined) {
                  onEdit();
                } else  {
                  handleMouse;
                }
              }}
            >
              Editar
            </Button>
         }

          <Button
            disableElevation
            sx={{
              backgroundColor: `${!!!isFormDisabled ? '#afda5f' : 'none'}`,
              mixBlendMode: 'multiply',
              color: '#272727'
            }}
            variant={!!!isFormDisabled ? 'contained' : 'outlined'}
            endIcon={<SaveIcon />}
            disabled={!!isFormDisabled ? isFormDisabled : false }
            onClick={()=>{
              if(onSave!== undefined) {
                onSave();
              } else  {
                handleMouse;
              }
            }}
          >
            Guardar
          </Button>
        
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default FormBar;