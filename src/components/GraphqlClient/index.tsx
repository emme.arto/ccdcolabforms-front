

const uriString= 'https://dev-cms.centroculturadigital.mx/admin/api'

import { ApolloClient, InMemoryCache } from '@apollo/client';

// Used server and client side - can't use react hooks

export const graphqlClient = new ApolloClient({
  cache: new InMemoryCache(),
  uri: uriString,
});
