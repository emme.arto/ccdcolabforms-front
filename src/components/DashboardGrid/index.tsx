import Box from '@mui/material/Box';
import DashboardItem from './DashboardItem';
import {DashboardGridProps} from '../../utils/types';



const DashboardGrid = ({list} : DashboardGridProps) => {
    return(
        <Box >
            <DashboardItem
                key={(Math.random() * 10000).toString()}
                titulo={`${'Titulo'}`}
                colaborador={`${'Colaborador'}`}
            />
            {
                list.map((item, index)=>{
                    // console.log(`Item no: ${index}`, item)
                    return(
                        <DashboardItem
                            key={item?.id?item?.id:(Math.random() * 10000).toString()}
                            colaborador={item?.usuarioCreador[0]?.nombre?item?.usuarioCreador[0]?.nombre:''}
                            titulo={item?.titulo?item?.titulo:''}
                            slug={item?.slug?item?.slug:'/dashboard'}
                        />
                    )
                })
            }
        </Box>
    )

}



export default DashboardGrid;
