import Link from 'next/link';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import {DashboardItemProps} from '../../utils/types';
import { maxCharactersDescription } from '@/utils';
import Divider from '@mui/material/Divider';


const DashboardItem = ( props : DashboardItemProps ) => {
    // console.log(props)
    const { slug, titulo, colaborador } = props;
    return( 
            <Box  sx={{
                margin:'0.5rem 0 0',
                // borderBottom: '1px solid gray',
                // textAlign:'center'
            }}>
            {!!slug ? (
                <Box sx={{textAlign:'center', width:'100%'}}>
                    <Paper elevation={0} >
                        <Button fullWidth href={`/colaboraciones/${slug}`} >
                        <Grid container sx={{padding:'1rem 0', width:'100%',alignItems:'center',}}>
                            <Grid item xs={4}><Typography variant={'body2'} sx={{fontSize:'1.2rem'}}>{`${colaborador}`}</Typography></Grid>
                            <Grid item xs={4}><Typography variant={'body1'} sx={{fontSize:'1.2rem'}}>{`${maxCharactersDescription(titulo, 40)}`}</Typography></Grid>
                            <Grid item xs={4}><Typography variant={'body2'} sx={{fontSize:'1.2rem'}}>{`${'Ver'}`}</Typography></Grid>
                        </Grid>
                        
                        </Button>
                        <Divider variant='middle' sx={{margin:'0 2rem'}}/>
                    </Paper>
                </Box>
            ) : <Box sx={{textAlign:'center', width:'100%'}}>
                    <Paper elevation={0} >
                        {/* <Grid container sx={{m:{lg:'8px 80px', xs:'8px 12px', sm:'8px 24px'}, alignItems:'center'}}> */}
                        <Grid container sx={{alignItems:'center'}}>
                            <Grid item xs={4}><Typography variant={'body1'}>{`Colaborador`}</Typography></Grid>
                            <Grid item xs={4}><Typography variant={'body1'}>{`Nombre del formulario`}</Typography></Grid>
                            <Grid item xs={4}><Typography variant={'body1'}>{``}</Typography></Grid>
                        </Grid>
                        <Divider variant='middle'sx={{margin:'0 2rem'}}/>
                    </Paper>
                </Box>}
            </Box>
    
    )
}

export default DashboardItem;