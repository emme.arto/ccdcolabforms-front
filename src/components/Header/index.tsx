import axios from 'axios';
import Link from 'next/link';
import Image from 'next/image';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import LogoutIcon from '@mui/icons-material/Logout';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import AssignmentIcon from '@mui/icons-material/Assignment';
import {useRouter} from 'next/router';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';


type HeaderProps = {
    isNew?: boolean
    isLogged?: boolean
    isDashboard?: boolean 
}
const Header = (props: HeaderProps) => {
    const router = useRouter();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.up('sm'));

    const logout = async () => {
        try {
            await axios.post('/api/unauth/unauth/');
            await axios.post('/api/unauth/logout/');
            router.push('/login')
    
        } catch(err) {
            console.error("ERROR: ", err);
            router.push('/login')
        }
    }

    return(
        <Box sx={{ flexGrow: 1}}>
            <AppBar position='relative' elevation={0} sx={{backgroundColor:'inherit', padding:'1rem 0'}}>
                <Toolbar disableGutters sx={{padding: {xs:'0rem 1rem 0rem',md:'0rem 1rem 0rem',lg:'0rem 0.8rem 0rem'}}}>
                <Grid container alignItems={'center'} rowSpacing={1} >
                    <Grid item xs={12} md={3} justifyContent={{xs:'center', md:'left'}} sx={{display:'grid'}}>
                        <Link href={'/'}>
                            <Image
                                width={120}
                                height={60}
                                alt={"logo de ccd"}
                                src={'/logo-ccd-black.svg'}
                            />
                        </Link>
                    </Grid>
                    <Grid item xs={4} md={3} justifyContent={{xs:'left', md:'center'}} sx={{display:'grid'}}>
                    {props.isNew !== true &&
                        (isMobile ? 
                        <Button
                            href='/formulario'
                            variant='text'
                            color='info'
                            startIcon={<AddCircleIcon />}
                        >
                            Crear Nuevo
                        </Button> :
                        <IconButton
                            href='/formulario'
                            color='info'
                        >
                            <AddCircleIcon/>
                        </IconButton>)
                    }
                    </Grid>
                    <Grid item xs={4} md={3} justifyContent={'center'} sx={{display:'grid'}}> 
                        {props.isDashboard === false && 
                            (isMobile ? 
                                <Button
                                    href='/dashboard'
                                    variant='text'
                                    color='info'
                                    startIcon={<AssignmentIcon />}
                                >
                                    Dashboard
                                </Button> :
                                <IconButton
                                    href='/dashboard'
                                    color='info'
                                >
                                    <AssignmentIcon/>
                                </IconButton>
                            )
                        }
                    </Grid>
                    <Grid item xs={4} md={3} justifyContent={{xs:'right'}} sx={{display:'grid'}}>
                            {isMobile ? 
                                <Button
                                    variant='outlined'
                                    color="info"
                                    endIcon={<LogoutIcon />}
                                    onClick={logout}
                                > Log out
                                </Button> :
                                <IconButton
                                    onClick={logout}
                                    color='info'
                                >
                                    <LogoutIcon />
                                </IconButton>
                            }
                    </Grid>
                </Grid>
                    
                
                    
                    

                                        
 
                    
                </Toolbar>
            </AppBar>
        </Box>
    )

}

export default Header;