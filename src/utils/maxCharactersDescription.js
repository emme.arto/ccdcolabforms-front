const maxCharactersDescription = (text, maxCharacters) => {
	const newText =
		!!text && !!text.length && text.length > maxCharacters
			? text.substring(0, maxCharacters) + '...'
			:  !text || !text.length
			? ''
			: text

	return newText
}

export default maxCharactersDescription
