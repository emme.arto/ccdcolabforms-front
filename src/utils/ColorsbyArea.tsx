import { snakeCase, upperFirst } from 'lodash'

const colors: Colors = {
    educativa: 'green',
    tecnologias_libres: 'orange',
    lab_web: 'lightBlue',
    e_literatura: 'purple',
    juegos: 'red',
    comunicacion: 'pink',
    inmersion: 'lightPurple',
    lab_de_tecnologia_y_lenguas_originarias: 'darkAqua',
    radio_ccd: 'oliva',
    cine_mas_alla: 'blue',
    default: 'default',
}

type Colors = {
	[name: string]: string
}

const ColorsbyArea = (name:string, prefix?: string) => {

    const areaColor = colors[`${snakeCase(name)}`] as string || colors.default
	return `${!!prefix ?  prefix: ''}${!!prefix ? upperFirst(areaColor): areaColor}`
}

export default ColorsbyArea
