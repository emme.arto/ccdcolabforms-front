import axios from 'axios';
import{useState, useEffect} from 'react';
import Header from '../../components/Header';
import DashboardGrid from '@/components/DashboardGrid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {AuthUser, DashboardType} from '../../utils/types';

const Dashboard = (props:DashboardType) => {
    const [colabsData, setColabsData] = useState([])
    const [loading, setLoading] = useState(false);
    const [userAuth, setUserAuth] = useState<AuthUser>(initUser)
    
    useEffect(()=>{
        const getProfile = async () => {
            const response = await axios.get('/api/auth/profile');
            console.log(response.data);
            setUserAuth(response.data);
        }

        getProfile();
    }, []);

    useEffect(()=> {
        const getColabs = async () =>{
            setLoading(true);
            try {
                const r = await axios.post('/api/talk/colabsDashboard', {
                    data: userAuth, 
                });
                setColabsData(r.data);
                setLoading(false);
            } catch(err) {
                setLoading(false);
                console.error("ERROR: ", err);
                // router.push('/login')
            }
        }
        getColabs();
    }, [userAuth])

    // console.log(colabsData);

    return (
    <>

        
        <Box sx={{margin:{lg:'2rem',md:'1rem', sm:'0.5rem', xs:'0'}}}>
            <Paper elevation={0} sx={{borderRadius:'16px', backgroundColor:''}}>
                <Header
                    isDashboard={true}
                />
                <Typography
                    paddingTop={4}
                    marginTop={2}
                    marginBottom={2}
                    marginLeft={{xs:4, md:7}} 
                    variant='h4'>
                    {`Hola ${userAuth.username !== "" ? (userAuth.username) : "Usuario"}`}
                </Typography>

                <Container sx={{padding:'1rem 0.25rem 2rem'}}>
                    {loading ?
                        <Box sx={{
                            display:'grid',
                            placeItems:'center'
                        }}>CARGANDO ... </Box >
                    : 
                        (colabsData?.length > 0) ?
                            <>
                                
                                <DashboardGrid list={colabsData}/> 
                            </>       
                    :<></>
                    }
                </Container>
            </Paper>  
        </Box>        
    </>)
}

const initUser = () : AuthUser => {
    return {
        email: "",
        username: "",
        id: "",
        tokenKeystone: ""
    }
}
// import profileHandler from '../api/auth/profile';
// export const getServerSideProps = async() => {

//     let user = initUser();
//     try {
//         const response = await axios.get('/api/auth/profile');
//         user = response?.data && response?.data;
//     } catch(err) {
//         console.error(err);
//     }
    
//     console.log("USUARIO: ", user);
//     return {
//       props: {
//             user: user
//         }
//     }
// }
  

export default Dashboard;