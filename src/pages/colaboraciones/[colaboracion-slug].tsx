import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import Header from '../../components/Header';
import Head from 'next/head'
import { InfoForm, FormBar, Toast } from '@/components';
import { createInfoType, infoType, userDataType } from '@/utils/types';

const Colaboracion = () => {
	const router = useRouter()
	const slug = router.query['colaboracion-slug']
	const [info, setInfo] = useState<infoType>(createInfoType());
	const [ disabled, setDisabled ] = useState<boolean>(true)
	const [loading, setLoading] = useState<boolean>(true);
	const [savedToast, setSavedToast] = useState(false);
	
	const handleSucccesToastClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
        return;
        }

        setSavedToast(false);
    };
	
	useEffect(()=>{
		setSavedToast(router.query.saved === "true" ? true: false)
		setLoading(true);
		const getInfo = async () => {
			const infoFetch = await axios.post('/api/talk/colabId',{
				data:{
					slug:slug
				}
			})
			// console.log(infoFetch.data)
			setInfo(infoFetch.data[0]);
			setLoading(false);
		}
		getInfo();
	}, [router.query.saved, slug])

	return (
		<div style={{padding:'0.5rem 0', backgroundColor:'#CBF281'}}>
			<Head>
				<title key='page-title'>{slug}</title>
				{/* <meta property='og:image' content={image} key='image' /> */}
			</Head>
			<Toast isOpen={savedToast} message={'Información guardada correctamente'} handleClose={handleSucccesToastClose} isSuccesful time={3000}/>    
			{!!!loading ?
				<InfoForm
					isDisabled={true}
					data={info}
					isNew={false}
				>
					<Header
						isNew={false}
						isDashboard={false}
					/>
				</InfoForm> 
				: <div style={{display:'grid', placeItems:'center', width:'100%', height:'100vh'}}>CARGANDO</div>
			}
		</div>
	)
}

export default Colaboracion
