import jwt, {Secret} from 'jsonwebtoken';
import axios from 'axios'
import {serialize} from 'cookie'

// const TOKEN_SECRET='secret'
// const API_URL = 'https://dev-cms.centroculturadigital.mx/admin/api';
const TOKEN_SECRET = process.env.TOKEN_SECRET  || "";
const API_URL = process.env.API_URL  || "";

export default async function loginHandler(req : any, res : any) {
    const { email, password } = req.body;
    // console.log(email, password)
    // if(!!email) 
    // if(password === passwordDB)
    
    const mutationQuery = {
        "query": `mutation signin($identity: String, $secret: String) {
            authenticate: authenticateUsuarioWithPassword(email: $identity, password: $secret) {
                token
                item {
                    id
                    nombre
                    email
                    rol
                }
            }
          }`,
        "variables": {
            "identity": `${email}`,
            "secret": `${password}`
        }
    };

    try {
        const fetching = await axios({
            url: API_URL,
            method: 'post',
            headers: {
                "content-type": "application/json",
            },
            data: mutationQuery
        })
        
        if(fetching.status === 200) {
            if(fetching.data.data.authenticate !== null) {
                const myToken :string = fetching.data?.data?.authenticate?.token ? fetching.data.data.authenticate.token : "";
                const myId :string = fetching.data?.data?.authenticate?.item?.id ? fetching.data.data.authenticate.item.id : '';
                const myName :string = fetching.data?.data?.authenticate?.item?.nombre ? fetching.data.data.authenticate.item.nombre : '';
                const myEmail :string = fetching.data?.data?.authenticate?.item?.email ? fetching.data.data.authenticate.item.email : '';
                // console.log ("MY ID: ", myId);
                // console.log("MY NAME: ", myName);
                // console.log("MY Email: ", myEmail);
                // console.log ("MY Token: ", myToken);
    
                
                const token2 = jwt.sign({
                    //// Tiempo a partir de AHORA * numero de segundos * numero de minutos * numero de horas en el dia * numero de dias
                    // exp: Math.floor(Date.now()/1000) * 60 * 60 * 23 * 30,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
                    email: myEmail,
                    username: myName,
                    id: myId,
                    tokenKeystone: myToken, 
                
                }, TOKEN_SECRET)
                
                const serializedToken = serialize('tokenAuthSerial', token2, {
                    httpOnly: true,
                    secure: process.env.NODE_ENV === 'production',
                    maxAge: 1000 * 60 * 60 * 24,
                    path: '/',
                    sameSite: 'lax'
                })
                // console.log(serializedToken);
                
                return res.status(200).setHeader('Set-Cookie', serializedToken).json(`Login Succesfuly ${myName} ${myEmail}`);
    
            } else {
                return res.status(401).json({error: 'Username or password incorrect'})
            }  
        }
        
    } catch(err) {
        console.error("ERROR en AXIoS:",err);
        return res.status(401).json({error: 'Error en axios'})
    }

}