import {verify} from 'jsonwebtoken';
import { extendedJWT } from '@/utils/types';
// const TOKEN_SECRET='secret';
const TOKEN_SECRET = process.env.TOKEN_SECRET || "";


export default function profileHandler(req:any, res:any) {
    const { tokenAuthSerial} = req.cookies; 

    if(!tokenAuthSerial) {
        return res.status(401).json({error: 'No token'})
    }

    const extendUse = (verifiedToken : any): extendedJWT => {
        const original = verify(verifiedToken, TOKEN_SECRET);
        if(typeof original === 'object') {
            return { 
                ...original,
                email: original.email ? original.email : '',
                username: original.username ? original.username : '',
                id: original.id ? original.id : '',
                tokenKeystone: original.tokenKeystone ? original.tokenKeystone : '',
            }
        
            
        } else {
            return {
                key: original,
                email: '',
                username: '',
                id: '',
                tokenKeystone: '',
            }
        }
    }

    try {
        const user = extendUse(tokenAuthSerial) ;

        // console.log("USUARIO en el PR: extendedJWT  OFILE: ", user);
        /*eslint-disable*/
        return res.json({
            email: user.email,
            username: user.username,
            id: user.id,
            tokenKeystone: user.tokenKeystone,

        })

    } catch (error) {
        return res.status(401).json({error: 'Invalid token'})
    }
}