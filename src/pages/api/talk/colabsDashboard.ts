import axios from 'axios';

// const API_URL = 'https://dev-cms.centroculturadigital.mx/admin/api';
const API_URL = process.env.API_URL || "";

export default async function colabsHandler (req: any, res:any) {
    // console.log("ESTOY En EL COLAB HaNDLEr");
    // console.log(req.body);
    const headers = {
        "content-type": "application/json; charset=utf-8",
        "vary": "Origin",
        "Authorization" : `Bearer ${req.body.data.tokenKeystone}`,
    };
    const mutationQuery = {
        "query": `query GET_COLABS {
            allColaboraciones {
                    id
                    titulo
                    subtitulo
                    slug
                    usuarioCreador{
                        id
                        nombre
                    }
                }
            }`,
    };

    const newFetch = await axios({ 
        url: API_URL,
        method: 'post',
        headers: headers,
        data: mutationQuery

    }).then((res) => {
        return res.data.data
    })
    
    try {
        const data = newFetch;
        // console.log(data.allColaboraciones);
        return res.status(200).json(data.allColaboraciones);
    } catch (err){
        console.error(err)
        return res.status(400).json("ERROR");
    }
}