import {verify} from 'jsonwebtoken';
import axios from 'axios';
import { extendedJWT } from '@/utils/types';

// const TOKEN_SECRET='secret';
// const API_URL = 'https://dev-cms.centroculturadigital.mx/admin/api';
const TOKEN_SECRET = process.env.TOKEN_SECRET || "";
const API_URL = process.env.API_URL || "";

export default async function colabIdHandler (req: any, res:any) {
    // console.log("ESTOY EN EL COLAB  ID HANDLER");
    const { tokenAuthSerial} = req.cookies; 
    if(!tokenAuthSerial) {
        return res.status(401).json({error: 'No token'})
    }

    const extendUse = (verifiedToken : any): extendedJWT => {
        const original = verify(verifiedToken, TOKEN_SECRET);
        if(typeof original === 'object') {
            return { 
                ...original,
                email: original.email ? original.email : '',
                username: original.name ? original.name : '',
                id: original.id ? original.id : '',
                tokenKeystone: original.tokenKeystone ? original.tokenKeystone : '',
            }
        
            
        } else {
            return {
                key: original,
                email: '',
                username: '',
                id: '',
                tokenKeystone: '',
            }
        }
    }

    try {
        const user = extendUse(tokenAuthSerial) ;
        const slug = req.body.data.slug;
        const headers = {
            "content-type": "application/json; charset=utf-8",
            "vary": "Origin",
            "Authorization" : `Bearer ${user.tokenKeystone}`,
        };
        const colabSlugQuery = {
            "query": `query GET_COLAB_SLUG($where: ColaboracionWhereInput){
                allColaboraciones(where: $where) {
                    usuarioCreador{
                        id
                        nombre
                    }
                    id
                    titulo
                    subtitulo
                    slug
                    tipoActividad
                    tipoActividadOtro
                    modo
                    personaOrganizadora
                    personasObjetivo
                    cupo
                    tieneRegistro
                    fechaInicioGlobal
                    fechaFinalGlobal
                    resumenSesiones
                    copy
                    introduccion
                    contenido
                    semblanza
                    contacto
                    urlDriveImages
                    urlDriveLogos
                    comentarios
                    requerimientosMobiliario
                    requerimientosInternet
                    requerimientosHardware
                    requerimientosSoftware
                    requerimientosPapeleria
                    requerimientosOtro
                    requerimientosOtroExtras
                    notas
                }
            }`,
            "variables" : {
                "where" : {
                    "slug" : `${slug}`
                }
            }            
        };

        const newFetch = await axios({ 
            url: API_URL,
            method: 'post',
            headers: headers,
            data: colabSlugQuery

        }).then((res) => {
            return res.data.data
        })
        
        const data = newFetch;
        // console.log(data.allColaboraciones);
        return res.status(200).json(data.allColaboraciones);
    } catch (err){
        console.error("ERROR EN COLAB ID: ", err)
        return res.status(400).json("ERROR");
    }
}