import { InfoForm, Header } from '../../components';
import Head from 'next/head';
import { createInfoType } from '@/utils/types';
import axios from 'axios';

const Formulario = () => {
    return(
    <div>
        <Head>
			<title key='page-title'>{"Nueva colaboración"}</title>
			{/* <meta property='og:image' content={image} key='image' /> */}
		</Head>
        
        <InfoForm
            data={createInfoType()}
            isDisabled={false}
            isNew={true}
           > 
            {<Header
                isNew={true}
                isDashboard={false}
            />}
        </InfoForm>
        
    </div>
    )
}

export default Formulario;