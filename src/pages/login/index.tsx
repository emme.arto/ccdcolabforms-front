import { Intro, LoginForm, Toast } from '../../components';
import Grid from '@mui/material/Grid';
import { getCookie } from 'cookies-next';
const Login = () => {
    // console.log("cookies", getCookie('tokenAuthSerial'))
    return (
    <Grid 
        container
        rowSpacing={6}
        sx={{marginTop:{xs:'0.2rem', md:'0.5rem'}}} >
        <Grid item xs={12} md={6}> <Intro/> </Grid>
        <Grid item xs={12} md={6}> <LoginForm/> </Grid>
    </Grid>
        
    )
}

export default Login;