import { gql } from "@apollo/client";

export const AUTH_USER = gql `
mutation signin($identity: String, $secret: String) {
  authenticate: authenticateUsuarioWithPassword(email: $identity, password: $secret) {
    token
    item {
      id
      nombre
      email
      rol
    }
  }
}
`

export const LOAD_COLABORACIONES = gql `
query GET_COLABS {
          allColaboraciones {
            	id
							titulo
							subtitulo
							slug
						}
          }
`

// export const LOAD_COLABORACIONES = gql `
// query GET_COLABS($where: ColaboracionWhereInput){
//           allColaboraciones(where: $where) {
//             	id
// 							titulo
// 							subtitulo
// 							slug
// 						}
//           }
// `
// export const LOAD_CONVOCATORIAS = gql`
//   query LOAD_CONVOCATORIAS($skip: Int, $where: ConvocatoriaWhereInput!) {
//     allConvocatorias(
//       first: 9
//       skip: $skip
//       sortBy: fechaInicio_DESC
//       where: $where
//     ) {
//       id
//       titulo
//       slug
//       categorias {
//         nombre
//       }
//       imagenCabecera
//       imagenDestacada
//       fechaInicio
//       fechaCierre
//       extracto
//     }
//     _allConvocatoriasMeta(where: $where) {
//       count
//     }
//   }
// `;

// export const LOAD_ENTRADAS_PASADAS_CINE_MAS_ALLA = gql`
//   query ENTRADAS_PASADAS_CINE_MAS_ALLA(
//     $skip: Int
//     $where: ActividadWhereInput!
//   ) {
//     allActividades(
//       first: 12
//       skip: $skip
//       sortBy: fechaFinalGlobal_DESC
//       where: $where
//     ) {
//       id
//       titulo
//       slug
//       imagenMediana
//       introduccion
//       subtitulo
//       fechaInicioGlobal
//       fechaFinalGlobal
//       clasificacion
//       categorias {
//         nombre
//       }
//       sesiones(sortBy: fechaInicio_DESC) {
//         fechaInicio
//         fechaFinal
//         descripcion
//       }
//     }
//     _allActividadesMeta(sortBy: fechaFinalGlobal_DESC, where: $where) {
//       count
//     }
//   }
// `;