/** @type {import('next').NextConfig} */
const path = require('path')
const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  env: {
    TOKEN_SECRET:'contraseña-ultrasecreta-ccdlaabweb',
    API_URL:'https://dev-cms.centroculturadigital.mx/admin/api',
  }
}

module.exports = nextConfig